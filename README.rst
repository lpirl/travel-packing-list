(More personal notes than anything else but stored publicly in case it
is of any use for anyone.)

----

`"opt out" travel packing list <https://lpirl.gitlab.io/travel-packing-list>`__
===============================================================================

We compile `different output formats
<https://lpirl.gitlab.io/travel-packing-list>`__ from the CSV file in
this repository.

The list is not intended to be definitive, but rather to be a list of
suggestions.

It can be used as a template to create a packing list for a specific
occasion (e.g., copy the list and rule out what you don't need).

Feel free to suggest changes.
