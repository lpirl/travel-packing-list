IN ?= list.csv
OUT_DIR ?= generated
INDEX ?= $(OUT_DIR)/index.html
CLEANED ?= $(OUT_DIR)/0-all.csv
TAGS ?= $(OUT_DIR)/0-tags.txt


all: $(TAGS) $(CLEANED:.csv=.txt) $(CLEANED:.csv=.html)
	grep '[^[:space:]]' $(TAGS) \
	|| exit 0 \
	&& $(MAKE) \
		$(patsubst %,$(OUT_DIR)/%.csv,$(shell cat $(TAGS) 2>/dev/null)) \
		$(patsubst %,$(OUT_DIR)/%.txt,$(shell cat $(TAGS) 2>/dev/null)) \
		$(patsubst %,$(OUT_DIR)/%.html,$(shell cat $(TAGS) 2>/dev/null))
	$(MAKE) $(INDEX)


$(OUT_DIR):
	mkdir -p $@


.PHONY: $(INDEX)
$(INDEX):
	rm -f $@
	echo "<html>" >> $@
	echo "<head>" >> $@
	echo "<title>\"opt out\" travel packing lists</title>" >> $@
	echo "</head>" >> $@
	echo "<body>" >> $@
	echo "<h1>packing lists</h1>" >> $@
	echo "<ul>" >> $@
	cd $(OUT_DIR) \
	&& find -mindepth 1 -maxdepth 1 -printf '%P\0' \
	| xargs -0IX echo "<li><a href='X'>X</a></li>" \
	| sort \
	>> ../$@
	echo "</ul>" >> $@
	echo "</body>" >> $@
	echo "</html>" >> $@


$(CLEANED): $(IN) $(OUT_DIR)
	@# egrep: remove comments
	@# sed: un-break continued lines (non-standard CSV)
	cat $< \
	| egrep -v '^\s*#' \
	| sed -E ':a;N;$$!ba;s/\n\s+/ /g' \
	> $@

$(TAGS): $(CLEANED)
	@# grep: only get lines with three fields
	@# cut: get third field
	@# grep: only get lines containing tags
	@# sed: split by spaces, make them newlines
	@# sed: remove hash
	@# sort: sort
	cat $< \
	| grep ';.*;' \
	| cut -d ';' -f 3 \
	| grep '#' \
	| sed -E 's/[[:space:]]+/\n/g' \
	| sed -E 's/^#//' \
	| sort -u \
	> $@


$(OUT_DIR)/%.csv: $(CLEANED)
	head -n1 $< > $@
	egrep '#$*(\s+|$$)' $< >> $@


%.rst: %.csv
	echo                       > $@
	echo '.. csv-table::'      >> $@
	echo '  :file: $<'         >> $@
	echo '  :delim: ;'         >> $@
	echo '  :header-rows: 1'   >> $@


%.html: %.rst
	cat $< \
	| rst2html \
	> $@


%.txt: %.csv
	cat $< \
	| tail -n+2 \
	| cut -d ';' -f1 \
	> $@


clean:
	rm -fr $(OUT_DIR)
